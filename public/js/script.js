var imageInstance = function (imageSrc, imageAlt) {
  this.imageBase64 = imageSrc;
  this.imageName = imageAlt;
};
var imageInstances = [];
var submitBtn = document.getElementById("sendData");
var stringValue = 'testString';

/*SUBMIT DATA*/
submitBtn.addEventListener("click", function(event) {


      var userSurname = document.getElementById("userDataSurname").value;
      var userFirstname = document.getElementById("userDataFirstname").value;
      var userPatronymic = document.getElementById("userDataPatronymic").value;
      var userPosiotion = document.getElementById("userDataPosition").value;
      if(imageInstances[0] !== undefined) {
        var userPhoto = imageInstances[0].imageBase64;
      } else {
        alert("Choose an user photo!");
      }
      var userPhoneNumber = document.getElementById("userPhoneNumber").value;
      var userEmail = document.getElementById("userEmail").value;
      var companyAdress = document.getElementById("companyAdress").value;
      var companyName = document.getElementById("companyName").value;
      if(imageInstances[1] !== undefined) {
        var companyLogo = imageInstances[1].imageBase64;
      } else {
        alert("Choose a company logo!");
      }
      var dispatchDate = document.getElementById("datepicker").value;
      var submit = true;
      /*APPEND DATA*/
      if(userSurname.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userFirstname.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userPatronymic.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userPosiotion.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userPhoto.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userPhoneNumber.value == ""){

        alert("false data!");
        submit = false;

      }
      if(userEmail.value == ""){

        alert("false data!");

      }
      if(companyAdress.value == ""){

        alert("false data!");
        submit = false;

      }
      if(companyName.value == ""){

        alert("false data!");
        submit = false;

      }
      if(companyLogo.value == ""){

        alert("false data!");
        submit = false;

      }
      if(dispatchDate.value == ""){

        alert("false data!");
        submit = false;

      }
      if(submit) {
        var userDataObj = new Object();
        userDataObj.surname = userSurname;
        userDataObj.firstname = userFirstname;
        userDataObj.patronymic = userPatronymic;
        userDataObj.position = userPosiotion;
        userDataObj.photo = userPhoto;
        userDataObj.phonenumber = userPhoneNumber;
        userDataObj.email = userEmail;
        userDataObj.adress = companyAdress;
        userDataObj.name = companyName;
        userDataObj.logo = companyLogo;
        userDataObj.date = dispatchDate;
        var userData = JSON.stringify(userDataObj);

      /*END APPEND DATA*/

          var xhr = new XMLHttpRequest();
          xhr.open("post","/send");
          xhr.setRequestHeader('Content-Type', 'application/json');
          xhr.send(userData);
          xhr.onreadystatechange = function() {
          if (xhr.readyState == 4) {
             if(xhr.status == 200) {
               stringValue = xhr.responseText;
               console.log(stringValue);
                 }
              }
          };
      }
});
/*END SUBMIT DATA*/


/*-------------------------------*/
var userPhotoForm = document.getElementById("userPhoto");
var userPhotoImage = document.getElementById("userPhotoImg");
getImages(userPhotoForm, userPhotoImage);
var companyLogoForm = document.getElementById("companyLogo");
var companyLogoImage = document.getElementById("companyPhotoImg");
getImages(companyLogoForm, companyLogoImage);

function getImages(form, image){
  form.addEventListener("change", function(event) {
    var photosArray = this.files;
    for(var i = 0; i < photosArray.length; i++) {
      preview(photosArray[i],image, form);
    }
  });
}
function preview(file, imageSrc, parentForm) {
  if(file.type.match(/image.*/)) {
    var reader = new FileReader();
    reader.addEventListener("load", function(event) {
      userPhotoClass = new imageInstance(event.target.result, file.name);
      if(parentForm.id == userPhotoForm.id) {

        imageInstances[0] = userPhotoClass;

      } else if(parentForm.id == companyLogoForm.id) {

        imageInstances[1] = userPhotoClass;

      } else {

        alert("error!");

      }
    
      imageSrc.src = event.target.result;
      imageSrc.alt = file.name;
    });
    reader.readAsDataURL(file);
  }
}
