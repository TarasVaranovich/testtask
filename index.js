var express = require('express'); // packege for creating web server
var moment = require('moment'); // package for getting current date
var fs = require("fs"); // for work with files (file system)
var mysql = require('mysql'); // package for work with mySQL database
var bodyParser = require('body-parser');// for parsing json data from post
var app = express();//node js express server

//PHANTOM
var path = require('path');
var childProcess = require('child_process');

//var phantomjs = require('./node_modules/phantomjs-prebuilt/bin/phantomjs');
var phantomjs = require('phantomjs-prebuilt'); //true path
var binPath = phantomjs.path;
console.log("Path:" + binPath);

var childArgs = [ // true path
  path.join(__dirname, 'phantomtest.js')
]

childProcess.execFile(binPath, childArgs, function(err, stdout, stderr) { // is running

    console.log(err);
    console.log(stdout);
    console.log(stderr);

})
//END PHANTOM

//REQUESTS GETIING AND SENDING DATA
var Logger = function (req, res, next) {
  var dateString = moment().format('MMMM Do YYYY, h:mm:ss a');
  var reqString = req.url;
  console.log("- LOG -->" + dateString + " query string:" + reqString);
  next(); // that prevent a server blocking
}
app.use(Logger);
app.use(express.static('public'));
//app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json({limit:'50mb'}));

app.post('/send', function(req, res){
  //console.log(req.body);
  var reqData = JSON.stringify(req.body);
  transferReq(reqData);
  console.log(req.headers);
  res.sendStatus(200);
});

function transferReq(jsonData) {
  var transferData = JSON.parse(jsonData);
  var userSurname = transferData.surname;
  var userFirstname = transferData.firstname;
  var userPatronymic = transferData.patronymic;
  var userPosiotion = transferData.position;
  var userPhoto = transferData.photo;
  var userPhoneNumber = transferData.phonenumber;
  var userEmail = transferData.email;
  var companyAdress = transferData.adress;
  var companyName = transferData.name;
  var companyLogo = transferData.logo;
  var dispatchDate = transferData.date;
}
//END REQUESTS GETIING AND SENDING DATA
//WORK WIDTH DATABASE
var server = app.listen(3000, function() {

    var host = server.address().address;
    var port = server.address().port;
    console.log('Express is listening at host: http://%s:port:%s', host, port);
});


var connection = mysql.createConnection({
    host     : 'localhost',
    port     : '3306',
    user     : 'root',
    password : 'mySQLroot',
    database : 'people'
});

connection.connect(function(err) {
  console.log("Connection is opened.");
});

var queryString = 'SELECT * FROM user';

connection.query(queryString, function(err, rows, fields) {
    if (err) throw err;

    for (var i in rows) {
        console.log('Post Titles: ', rows[i].firstName);
    }
});

connection.end(
function(err) {
  if(!err) {
    connection = null;
    console.log("Connection is closed.");
  }
  else {
    console.log(err);
  }
});
console.log("-End of file-");
//END WORK WITH DATABASE
